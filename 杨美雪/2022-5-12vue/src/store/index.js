import Vuex from 'vuex';
import Vue from 'vue';
import mutations from './mutations'
import actions from './actions'
import user from './modules/user'
Vue.use(Vuex);
const store = new Vuex.Store({
    state: {  
         name:'外卖系统',
         cart:[],
         buyNums:0,
         money:0
     },    
 
     mutations,
     actions,
     
     getters:{
        getName:(state)=>{
          return state.name+'----我们的项目';
        }
     },
     modules:{
          user
     }
 
  })
  export default store;
 