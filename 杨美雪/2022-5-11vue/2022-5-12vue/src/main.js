import Vue from 'vue'
import App from './App.vue'
import axios from 'axios'
//全局引入
// import ElementUI from 'element-ui';
// import 'element-ui/lib/theme-chalk/index.css';
// Vue.use(ElementUI);

//按需引入
import {Button,Row,Input,Carousel,CarouselItem} from 'element-ui';

//全局注册
Vue.component(Button.name,Button);
Vue.component(Input.name,Input);
Vue.component(Carousel.name,Carousel);
Vue.component(CarouselItem.name,CarouselItem);
Vue.component(Row.name,Row);

Vue.component('class7-button',Button);

import VueScroller from 'vue-scroller'
Vue.use(VueScroller)
// 在js es2015，有优先级别，优先级别比较好的是 import
//引入VueRouter
import VueRouter from 'vue-router'
//应用插件
Vue.use(VueRouter)

import store from './store';
//引入路由器
import router from './router/index';



// axios.defaults.baseURL='http://123.207.32.32:8000';

Vue.prototype.$axios=axios;




//关闭生产环境提交
Vue.config.productionTip = false
//render 渲染 temldate.
new Vue({
  el:'#app',
  // template:'<App></App>',
  // components:{
  //   App,
  // }
  //脚手架引入的vue是残缺的，为了性能
  render: (createElement) => {
    // console.log(typeof createElement);
    return createElement(App)
   },
   beforeCreate:function(){
     Vue.prototype.$bus=this;
   },
   router,
   store
})
